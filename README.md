# visualization_coordinate_axis 
This repository is to visualize coordinate axis on RViz.  

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

**Content:**

*   [Preparation](#preparation)
*   [Execution](#execution)
*   [Files](#files)
*   [References](#References)


## Preparation
You need to store position data in `data` folder.  
Please confirm sample data in `data` folder.  

## Execution
You can confirm a result of visualization by executing the following code.  
In additon, I prepared dynamic coordinate axis.  
You will be able to confirm sample result on RViz.  

~~~
roslaunch visualization_coordinate_axis visualization_coordinate_axis.launch
~~~


## Files
 - `README.md`: Read me file (This file)

 - `visualization_coordinate_axis.launch`
 
 - `dynamic_tf2_broadcaster.py`: Visualization of dynamic coordinate axis

 - `fixed_tf2_broadcaster.py`: Visualization of static coordinate axis

 - `__init__.py`: Code for initial setting (PATH and parameters)
 
## Reference
